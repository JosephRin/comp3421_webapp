const sqlite3 = require('sqlite3').verbose();

module.exports = 
{
  connect: () => 
  {
    return new sqlite3.Database('./public/webapp.db', (err) => {
      if (err) {
        return console.error(err.message);
      }
      console.log('Connected to the SQlite database.');
    });
  },
  disconnect: (db) =>
  {
    db.close((err) => {
      if (err) {
        return console.error(err.message);
      }
      console.log('Close the database connection.');
    });
  }
}