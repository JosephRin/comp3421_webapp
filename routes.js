var express = require('express');
var session = require('express-session');
var router = express.Router();
var db = require('./db');

router.post('/login', (req, res) =>
{
    var username = req.body.urn;
    var password = req.body.pwd;
    var conn = db.connect();
    conn.get('SELECT user_id, username FROM account WHERE username=? AND password=?', 
    [
        username,
        password
    ], (err, rows) =>
    {
        if(err)
        {
            return console.error(err.message);
        }
        res.send(rows);
    });
    db.disconnect(conn);
})

router.post('/register', (req, res) => {
    var username = req.body.username;
    var password = req.body.password;
    var conn = db.connect();
    var query = 'SELECT EXISTS(SELECT 1 FROM account WHERE username=?) as exist';
    conn.get(query, [username], (err, rows) => {
        if(err){
            return console.log(err.message);
        }
        if(rows.exist == 1){
            res.send("This username has already existed.");
            return false;
        }
        query = 'INSERT INTO account (username, password) VALUES (?, ?)';
        conn.run(query, [username, password], (err, rows) => {
            if(err){
                return console.log(err.message);
            }
            res.send("Registration Successful.");
        })
    })
    db.disconnect(conn);
})

router.post("/view_answer", (req, res) => {
    var user_id = req.body.user_id;
    var question_id = req.body.question_id;
    var conn = db.connect();
    var query = 'SELECT EXISTS(SELECT 1 FROM exercise WHERE user_id=? AND question_id=?) as exist';
    conn.get(query, [user_id, question_id], (err, rows) => {
        if(err) return false;
        if(rows.exist == 1){
            query = 'SELECT answer FROM exer_question WHERE question_id=?';
            conn.get(query, [question_id], (err, rows) => {
                if(err) return console.log(err.message);
                res.send(rows);
            })
        }else{
            res.send("false");
        }
    })
    db.disconnect(conn);
})

router.post('/exer_submit', (req, res) =>
{
    var user_id = req.body.user_id;
    var answer = req.body;
    // console.log(level, answer);
    var ft_question = Object.keys(answer)[0];
    var conn = db.connect();
    var query = 'SELECT EXISTS(SELECT 1 FROM exercise WHERE user_id=? AND question_id=?) as exist';
    conn.get(query, [user_id, ft_question], (err, rows) => {
        if(err)
        {
            return console.error(err.message);
        }
        if(rows.exist == 1){
            res.send("You have already answered.");
            return false;
        }
        Object.keys(answer).forEach(function(question){
            if(question != "user_id"){
                conn.get(query, [user_id, question], (err, rows) => {
                    query = 'INSERT INTO exercise (user_id, question_id, answer) VALUES (?, ?, ?)';
                    conn.run(query, [
                        user_id,
                        question,
                        answer[question]
                    ], (err) => {
                        if(err)
                        {
                            return console.error(err.message);
                        }
                    })
                })
            }
        })
        res.send("Answer successfully.");
    });
    db.disconnect(conn);
})

router.get('/report', (req, res) => {
    var conn = db.connect();
    var question_id = req.query.question_id;
    var query = 'SELECT answer FROM exercise WHERE question_id = ?';
    conn.all(query, [question_id], (err, rows) => {
        if(err) return false;
        var data = [];
        for(var record in rows){
            data.push(rows[record].answer);
        }
        res.send(data);
    })
    db.disconnect(conn);
})

module.exports = router;