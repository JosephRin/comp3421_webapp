// --- global variable ---

var timer = [];
var list = [];
var id_index = 1; 

//--- for sorting simulator ---

function setTag(id, arr)
{
	var i, id_tag;
	for(i=0;i<arr.length;i++)
	{
		id_tag = id.toString() + "-" + i.toString() + "-text";
		document.getElementById(id_tag).innerHTML = arr[i];
	}
}

function setSwap(id, swap_prev, swap_curr, check_prev, check_curr, isSwap)
{

	var i;
	for(i=0; i<swap_prev.length; i++)
	{
		tag = id.toString() + "-" + swap_prev[i].toString() + "-box";
		document.getElementById(tag).classList.remove("swap", "check");
	}
	for(i=0; i<check_prev.length; i++)
	{
		tag = id.toString() + "-" + check_prev[i].toString() + "-box";
		document.getElementById(tag).classList.remove("swap", "check");
	}

	for(i=0; i<check_curr.length; i++)
	{
		tag = id.toString() + "-" + check_curr[i].toString() + "-box";
		document.getElementById(tag).classList.remove("swap", "check");
		document.getElementById(tag).classList.add("check");
	}
	if(isSwap)
	{
		for(i=0; i<swap_curr.length; i++)
		{
			tag = id.toString() + "-" + swap_curr[i].toString() + "-box";
			document.getElementById(tag).classList.remove("swap", "check");
			document.getElementById(tag).classList.add("swap");
		}	
	}
}

function reset(id, n)
{
	for(i=0;i<n;i++)
	{
		id_tag = id.toString() + "-" + i.toString() + "-box";
		document.getElementById(id_tag).classList.remove("swap", "check", "finish");
	}
}

function setAll(id, n)
{
	for(i=0;i<n;i++)
	{
		id_tag = id.toString() + "-" + i.toString() + "-box";
		document.getElementById(id_tag).classList.remove("swap", "check");
		document.getElementById(id_tag).classList.add("finish");
	}
}

function getValues(idx, arr)
{
	var i, result = [];
	for(i=0; i<idx.length; i++)
	{
		result.push(arr[idx[i]]);
	}
	return result;
}

function getSort(id)
{
	var i = 0;
	while(list[i].id != id)
	{
		if(list[i].id > id)	return null;
		i++;
	}
	return list[i];
}

function btn_return(id)
{
	var sort = getSort(id);
	if(sort == null) return null;
	sort.step = 0;
	setTag(sort.id, sort.input);
	reset(sort.id, sort.count);
	setDescription(sort.id, -1, false, [], []);
	btn_pause(id);
	return true;
}

function btn_result(id)
{
	var sort = getSort(id);
	if(sort == null) return null;
	sort.step = sort.maxStep;
	setTag(sort.id, sort.output);
	setAll(sort.id, sort.count);
	setDescription(sort.id, -2, false, [], sort.output);
	btn_pause(id);
	return true;
}

function btn_prev(id)
{
	var sort = getSort(id);
	if(sort == null) return null;
	if(sort.step <= 0) return false;
	if(sort.step == sort.maxStep)
	{
		reset(sort.id, sort.count);
	}
	sort.step -= 1;
	setTag(sort.id, sort.iteration.result[sort.step]);
	setDescription(sort.id, sort.step, sort.iteration.isSwap[sort.step], getValues(sort.iteration.swap[sort.step], sort.iteration.result[sort.step]), getValues(sort.iteration.check[sort.step], sort.iteration.result[sort.step]));
	setSwap(id, sort.iteration.swap[sort.step+1], sort.iteration.swap[sort.step], sort.iteration.check[sort.step+1], sort.iteration.check[sort.step], sort.iteration.isSwap[sort.step])
	return true;
}

function btn_next(id)
{
	var sort = getSort(id);
	if(sort == null) return false;
	if(sort.step >= sort.maxStep) 
	{
		btn_result(id);
		return false;
	}
	sort.step += 1;
	if(sort.step == sort.maxStep)
	{
		btn_result(id);
		return true;
	}
	setTag(sort.id, sort.iteration.result[sort.step]);
	setDescription(sort.id, sort.step, sort.iteration.isSwap[sort.step], getValues(sort.iteration.swap[sort.step], sort.iteration.result[sort.step]), getValues(sort.iteration.check[sort.step], sort.iteration.result[sort.step]));
	setSwap(id, sort.iteration.swap[sort.step-1], sort.iteration.swap[sort.step], sort.iteration.check[sort.step-1], sort.iteration.check[sort.step], sort.iteration.isSwap[sort.step])
	return true;
}

function btn_play(id)
{
	timer.splice(id-1, 1, setInterval(function(){
		btn_next(id);
	}, 850));
	document.getElementById(id+"-btn-pause").classList.remove("disable");
	document.getElementById(id+"-btn-play").classList.add("disable");
}

function btn_pause(id)
{
	clearInterval(timer[id-1]);
	document.getElementById(id+"-btn-play").classList.remove("disable");
	document.getElementById(id+"-btn-pause").classList.add("disable");
}

function btn_close(id)
{
	btn_pause(id);
	document.getElementById(id).setAttribute("style", "display:none;");
}

function btn_save(id)
{
	var sort = getSort(id);
	if(sort == null) return null;

	var a = document.createElement("a");
	var file = new Blob([JSON.stringify(sort)], {type: 'text/plain'});
	a.href = URL.createObjectURL(file);
	a.download = "sorting-"+id+".txt";
	a.click();
}

function setDescription(id, step, isSwap, swapElements, checkElements)
{
	descr = document.getElementById(id.toString()+"-description");
	if(step == -1 || step == 0)
	{
		descr.innerHTML = "";
		return true;
	}
	else if(step == -2)
	{
		descr.innerHTML = '<div style="color: lightgreen;">All elements are sorted.<br><'+checkElements.toString()+'></div>';
		return true;
	}
	var str = '<div style="color: lightsalmon;">--- Iteration ' + step + ' ---</div>';

	var i;
	if(checkElements.length > 0)
	{
		str += '<div style="color: lightskyblue;">Compare <' + checkElements[0] + '> and <';
		for(i=1;i<checkElements.length-1;i++)
		{
			str += checkElements[i] + '> and <';		
		}
		str += checkElements[checkElements.length-1] + '></div>';
	}
	if(isSwap && swapElements.length > 0)
	{
		str += '<div style="color: lightcoral;">Swap <' + swapElements[0] + '> and <';
		for(i=1;i<swapElements.length-1;i++)
		{
			str += swapElements[i] + '> and <';		
		}
		str += swapElements[swapElements.length-1] + '></div>';
	}
	descr.innerHTML = str;
	return true;
}

function render(id)
{
	var sort = getSort(id);
	var svg_width = 600;
	var svg_height = 200;
	var i;
	if(sort == null) return null;
	timer.push(id);



	var content = '<div class="sorting" id="'+sort.id+'">'+
			'<div class="option">'+
				'<div class="btn-close btn-option" id="'+sort.id+'-btn-close" onclick="btn_close('+sort.id+')">X</div>'+
				'<div class="btn-save btn-option" id="'+sort.id+'-btn-save" onclick="btn_save('+sort.id+')">Save</div>'+
			'</div>'+
			'<div class="title">'+'#'+sort.id+' '+sort.name+'</div>'+
			'<div class="information">'+sort.description+ ', total steps: '+(sort.maxStep-1)+ '<br><'+ sort.input +'></div>'+
			'<div class="content">'+
				'<div class="description" id="'+sort.id+'-description">'+
				'</div>'+
				'<svg id="'+sort.id+'-svg" width="'+svg_width+'" height="'+svg_height+'">'+
					'<g class="data_container">';

	for(i=0; i<sort.count; i++)
	{
		var tag = sort.id + "-" + i;
		var rect_x = ((svg_width/sort.count)-50)/2 + (i * svg_width/sort.count);
		var text_x = rect_x+20;
		content += '<g class="data" id="'+tag+'-box">'+
							'<rect id="'+tag+'-rect" x="'+rect_x+'" y="80" width="50" height="50"/>'+
							'<text id="'+tag+'-text" x="'+text_x+'" y="120"></text>'+
						'</g>';
	}
						
	content += '</g>'+
				'</svg>'+
			'</div>'+
			'<div class="function">'+
				'<button id="'+sort.id+'-btn-return" onclick="btn_return('+sort.id+')">Reset</button>'+
				'<button id="'+sort.id+'-btn-prev" onclick="btn_prev('+sort.id+')">Prev</button>'+
				'<button id="'+sort.id+'-btn-play" onclick="btn_play('+sort.id+')">Play</button>'+
				'<button id="'+sort.id+'-btn-pause" class="disable btn-pause" onclick="btn_pause('+sort.id+')">Pause</button>'+
				'<button id="'+sort.id+'-btn-next" onclick="btn_next('+sort.id+')">Next</button>'+
				'<button id="'+sort.id+'-btn-result" onclick="btn_result('+sort.id+')">Result</button>'+
			'</div>'+
		'<div>';

	var tp = document.createElement("div");
	tp.innerHTML = content;
	document.getElementById("main").appendChild(tp);
	// document.getElementById("main").insertBefore(tp, document.getElementById("main").childNodes[0]);
	setTag(id, sort.input);
	goToView(document.getElementById(sort.id));
}

function goToView(element)
{
	element.scrollIntoView({behavior: "smooth", block: "center", inline: "center"});
}

// --- for sorting method ---

function bubbleSort(id, arr)
{
	var n = arr.length;
	var i,j, temp, step=0;

	var result = {
		id: id,
		name: "Bubble Sort",
		description: arr.length+" elements",
		method: "bubbleSort",
		input: arr.slice(),
		count: arr.length,
		step: 0,
		iteration: {
			isSwap: [false],
			swap: [[]],
			check: [[]],
			result: [arr.slice()]
		},
		maxStep: 0,
		output: []
	}

	for(i=0; i<n; i++)
	{
		for(j=0; j<n-i-1; j++)
		{
			if(arr[j] > arr[j+1])
			{
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
				result.iteration.isSwap.push(true);
				result.iteration.swap.push([j, j+1]);
				result.iteration.check.push([j, j+1]);
			}
			else
			{
				result.iteration.isSwap.push(false);
				result.iteration.swap.push([]);
				result.iteration.check.push([j, j+1]);
			}
			result.maxStep++;
			result.iteration.result.push(arr.slice());	
		}
	}
	result.iteration.swap.push([]);
	result.iteration.check.push([]);
	result.maxStep++;
	result.iteration.result.push(arr.slice());	
	result.output = arr;
	return result;
}


function quickSort(arr, left, right, qs_result){
   var len = arr.length, 
   pivot,
   partitionIndex;


  if(left < right){
    pivot = right;
    partitionIndex = partition(arr, pivot, left, right, qs_result);
    
   //sort left and right
   quickSort(arr, left, partitionIndex - 1, qs_result);
   quickSort(arr, partitionIndex + 1, right, qs_result);
  }
  return arr;
}

function partition(arr, pivot, left, right, qs_result){
   var pivotValue = arr[pivot],
       partitionIndex = left;

   for(var i = left; i < right; i++){
    if(arr[i] < pivotValue){
      swap(arr, i, partitionIndex);
      if(i == partitionIndex)
      {
      	qs_result.iteration.isSwap.push(false);
      	qs_result.iteration.swap.push([]);
      	qs_result.iteration.check.push([i, pivot]);
      }
      else
      {
      	qs_result.iteration.isSwap.push(true);
      	qs_result.iteration.swap.push([i, partitionIndex]);
      	qs_result.iteration.check.push([i, pivot]);
      }
      
      partitionIndex++;
    }
    else
    {
	    qs_result.iteration.check.push([i, pivot]);
	    qs_result.iteration.swap.push([]);
    	qs_result.iteration.isSwap.push(false);
    }
    qs_result.iteration.result.push(arr.slice());
    qs_result.maxStep += 1;
  }

  swap(arr, right, partitionIndex);
  if(right == partitionIndex)
  {
  	qs_result.iteration.isSwap.push(false);
  	qs_result.iteration.swap.push([]);
  	qs_result.iteration.check.push([right, partitionIndex]);
  }
  else
  {
  	qs_result.iteration.isSwap.push(true);
  	qs_result.iteration.swap.push([right, partitionIndex]);
  	qs_result.iteration.check.push([right, partitionIndex]);
  }
  qs_result.iteration.result.push(arr.slice());
  qs_result.maxStep += 1;
  return partitionIndex;
}

function swap(arr, i, j){
   var temp = arr[i];
   arr[i] = arr[j];
   arr[j] = temp;
}

function call_quicksort(id, arr)
{
	var qs_result = {
			id: id,
			name: "Quisk Sort",
			description: arr.length+" elements",
			method: "quicksort",
			input: arr.slice(),
			count: arr.length,
			step: 0,
			iteration: {
				isSwap: [false],
				swap: [[]],
				check: [[]],
				result: [arr.slice()]
			},
			maxStep: 0,
			output: []
		}
	var qs = quickSort(arr.slice(), 0, arr.length-1, qs_result);
	qs_result.iteration.swap.push([]);
	qs_result.iteration.check.push([]);
	qs_result.maxStep++;
	qs_result.iteration.result.push(qs.slice());	
	qs_result.output = qs;
	return qs_result;
}

function selectionSort(id, arr)
{
	var ss_result = {
		id: id,
		name: "Selection Sort",
		description: arr.length+" elements",
		method: "selection sort",
		input: arr.slice(),
		count: arr.length,
		step: 0,
		iteration: {
			isSwap: [false],
			swap: [[]],
			check: [[]],
			result: [arr.slice()]
		},
		maxStep: 0,
		output: []
	}


	for(var i=0; i<arr.length; i++){
        var mi = i, temp, flag = false, count_flag = false;
        
        for(var j = i + 1; j<arr.length; j++) {
        	count_flag = true;
            ss_result.iteration.isSwap.push(false);
            ss_result.iteration.check.push([mi, j]);
            ss_result.iteration.swap.push([]);
            ss_result.iteration.result.push(arr.slice());
            ss_result.maxStep += 1;
            if(arr[j] < arr[mi])
            {
            	if(j == arr.length-1)
            	{
            		if(mi == i)
            		{
            			ss_result.iteration.check[ss_result.iteration.check.length-1][0] = i;
            		}
            		else
            		{
            			ss_result.iteration.check[ss_result.iteration.check.length-1][1] = i;
            		}
            		flag = true;
            	}
                mi = j;
            }
        }
        temp = arr[i];
        arr[i] = arr[mi];
        arr[mi] = temp;
        if(count_flag)
        {
        	ss_result.iteration.isSwap.pop();
            ss_result.iteration.swap.pop();
            ss_result.iteration.result.pop();
            if(!flag)
	        {
	        	ss_result.iteration.check[ss_result.iteration.check.length-1][0] = i;
	        }
        }
        ss_result.iteration.isSwap.push(true);
        ss_result.iteration.swap.push([mi, i]);
        ss_result.iteration.result.push(arr.slice());
    }
	ss_result.iteration.swap.push([]);
	ss_result.iteration.check.push([]);
	ss_result.maxStep++;
	ss_result.iteration.result.push(arr.slice());	
	ss_result.output = arr;
	return ss_result;
}


// --- for basic function ---

function init()
{
	main = document.getElementById("main");
	main.innerHTML += '<div class="form"></div>';
}

function btn_create()
{
	var str = document.getElementById("num-field").value;
	var type = document.getElementById("sorting-method").selectedIndex;
	var numbers = str.split(",");
	var i;
	for(i=0; i<numbers.length; i++)
	{
		numbers[i] = parseFloat(numbers[i]);
	}

	if(type == 0)
	{
		//bubble sort, type = 0
		var b = bubbleSort(id_index, numbers.slice());
		list.push(b);
		render(id_index);
		id_index++;
	}
	else if(type == 1)
	{
		//selection sort, type = 2
		var s = selectionSort(id_index, numbers.slice());
		list.push(s);
		render(id_index);
		id_index++;
	}
	else if(type == 2)
	{
		//quick sort, type = 1
		var q = call_quicksort(id_index, numbers.slice());
		list.push(q);
		render(id_index);
		id_index++;
	}
}

function btn_clear() 
{
	document.getElementById("num-field").value = "";
}

$(document).ready(function(){
	$(".exer-form").submit(function(e){
		e.preventDefault();
		//validate
		var numberOfQuestions = $(this).attr("qnum");
		var answerdQuestions = $(this).serializeArray().length;
		if(answerdQuestions != numberOfQuestions){
			alert("Please answer all question(s).");
			return false;
		}

		var cookie = document.cookie, user_id;
		if( (cookie = getCookieJson(cookie)) !== ""){
			if(cookie.user_id === undefined) return;
			user_id = cookie.user_id;
		}else{
			alert("Please login.");
			return false;
		}
		// console.log($(this).serialize());
		$.ajax({
			data: $(this).serialize() + "&user_id=" + user_id,
			type: "POST",
			url: "/exer_submit",
			success: function(response) { 
					alert(response);
			}
		})
	})


	$(".view-ans").click(function(){
		var cookie = document.cookie, user_id;
		if( (cookie = getCookieJson(cookie)) !== ""){
			if(cookie.user_id === undefined) return;
			user_id = cookie.user_id;
		}else{
			alert("Please login.");
			return false;
		}

		var question_id = $(this).attr("question_id");
		question_id = question_id.split(",");
		for(var i in question_id){
			$.ajax({
				data: "question_id=" + question_id[i] + "&user_id=" + user_id,
				type: "POST",
				url: "/view_answer",
				success: function(response) { 
						if(response == "false"){
							alert("Please answer question(s) first.");
						}else{
							var answer = "";
							switch(response.answer){
								case 1:
									answer = "A";
									break;
								case 2:
									answer = "B";
									break;
								case 3:
									answer = "C";
									break;
								case 4:
									answer = "D";
									break;
							}
							alert("Answer is " + answer);
						}
				}
			})
		}
	})
})

// --- testing ---

// var array = [10,9,8,7,6,5,4,3,2,1];
// var bubble = bubbleSort(id_index++, array.slice());
// var quick = call_quicksort(id_index++, array.slice());
// list.push(bubble);
// list.push(quick);
// render(1);
// render(2);