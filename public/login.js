var cookie = document.cookie;
if( (cookie = getCookieJson(cookie)) !== ""){
    if(cookie.user_id !== undefined){
        $("#welcome").text("Welcome " + cookie.username + " ");
        $(".login-text").css("display", "none");
        $(".logout-text").css("display", "inline");
    }
}

function getCookieJson(cookie){
    var head = cookie.indexOf("{");
    var tail = cookie.indexOf("}") + 1;
    try{
        JSON.parse(cookie.substring(head, tail));
    }catch(e){
        return "";
    }
    return JSON.parse(cookie.substring(head, tail));
}

function btn_login()
{
    var cookie = document.cookie;
    if((cookie = getCookieJson(cookie)) !== ""){
        if(cookie.user_id !== undefined){
            logout();
            return;
        }
    }

    var login_layer = document.getElementById("login-layer");
    login_layer.style.display = "block";
    setTimeout(function(){
        window.addEventListener('click', setClose);
    }, 1)
}

var setClose = function(e){
    var login_container = document.getElementsByClassName("login-container")[0];
    if(!login_container.contains(e.target))
    {
        btn_login_close();
    }
}

function btn_login_cancel()
{
    btn_login_close();
}

function btn_login_close()
{
    clearHint();
    document.getElementById("login-layer").style.display = "none";
    window.removeEventListener('click', setClose);
}

function btn_login_login()
{
    //do some validation
    login();
}

function btn_login_register()
{
    var username = prompt("Please enter your username", "");
    if(username == null || username == "") return false;
    var password = prompt("Please enter your password: ", "");
    if(password == null || password == "") return false;
    var data = "username=" + username + "&password=" + password;
    $.ajax({
        data: data,
        type: "POST",
        url: "/register",
        success: function(response) { 
            alert(response);
        }
    })
}

function logout(){
    document.cookie = "";
    $("#welcome").text("");
    $(".login-text").css("display", "inline");
    $(".logout-text").css("display", "none");
}

function login()
{
    var username = document.getElementById("login-name-field").value;
    var password = document.getElementById("login-password-field").value;
    if(!checkUsername(username)) return false;
    if(!checkPassword(password)) return false;

    var path = "login";
    var params = {"urn": username, "pwd": password};
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function()
    {
        if(this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            if(result == "")
            {
                clearHint();
                alertUsername("Username/Password is/are not correct.");
                alertPassword("");
            }
            else
            {
                btn_login_close();
                loginSuccess(result);
            }
        }
    };
    xhttp.open("POST", path, true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.send(JSON.stringify(params));
}

function loginSuccess(user_info)
{
    user_info = JSON.parse(user_info);
    console.log(user_info);
    var cookie = {
        "user_id": user_info.user_id,
        "username": user_info.username
    }
    document.cookie = JSON.stringify(cookie);
    $("#welcome").text("Welcome " + user_info.username + " ");
    $(".login-text").css("display", "none");
    $(".logout-text").css("display", "inline");
}

function checkUsername(username)
{
    if(username == "")
    {
        clearHint();
        alertUsername("Please enter Username.");
        return false;
    }
    return true;
}

function checkPassword(password)
{
    if(password == "")
    {
        clearHint();
        alertPassword("Please enter Password.");
        return false;
    }
    return true;
}

function alertUsername(message)
{
    document.getElementById("login-name-field").classList.add("login-input-invalid");
    document.getElementById("login-hint").innerHTML += message;
}

function alertPassword(message)
{
    document.getElementById("login-password-field").classList.add("login-input-invalid");
    document.getElementById("login-hint").innerHTML += message;
}

function clearHint()
{
    document.getElementById("login-hint").innerHTML = "";
}

document.getElementById("login-password-field").addEventListener("keydown", function(e)
{
    if (e.keyCode == 13) { 
        btn_login_login(); 
        window.getSelection().removeAllRanges();
        document.activeElement.blur();
    }
})

document.getElementById("login-password-field").addEventListener("focus", function()
{
    this.classList.remove("login-input-invalid");
})

document.getElementById("login-name-field").addEventListener("keydown", function(e)
{
    if (e.keyCode == 13) { 
        btn_login_login(); 
        window.getSelection().removeAllRanges();
        document.activeElement.blur();
    }
})

document.getElementById("login-name-field").addEventListener("focus", function()
{
    this.classList.remove("login-input-invalid");
})