$(document).ready(function(){
    var renderReport = function(question_id, label, title, choice){
        var data = "question_id=" + question_id;
        $.ajax({
            data: data,
            type: "GET",
            url: "/report",
            success: function(response) { 
                if(choice == "TF"){
                    data = [0, 0];
                    for(var i in response){
                        data[response[i]-1] += 1;
                    }
                }else if(choice == "MT"){
                    data = [0, 0, 0, 0];
                    for(var i in response){
                        data[response[i]-1] += 1;
                    }
                }
                // console.log(data);
                createPieChart(label, data, title);
            }
        })
    }

    var createPieChart = function(label, data, title){
        var dom = document.createElement('canvas');
        dom.setAttribute("height", "400px");
        dom.setAttribute("width", "400px");
        document.getElementById("containter").appendChild(dom);
        var ctx = dom.getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: label,
                datasets: [{
                    label: '# of Votes',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                display: true,
                text: title
            }}
        });
    }

    var question_id = 1;
    var title = "Question 1 (Easy)";
    var label = ["False", "True"];
    var choice = "TF";
    renderReport(question_id, label, title, choice);

    var question_id = 2;
    var title = "Question 2 (Normal)";
    var label = ["False", "True"];
    var choice = "TF";
    renderReport(question_id, label, title, choice);

    var question_id = 3;
    var title = "Question 3 (Normal)";
    var label = ["A", "B", "C", "D"];
    var choice = "MT";
    renderReport(question_id, label, title, choice);

    var question_id = 4;
    var title = "Question 4 (Hard)";
    var label = ["A", "B", "C", "D"];
    var choice = "MT";
    renderReport(question_id, label, title, choice);
})